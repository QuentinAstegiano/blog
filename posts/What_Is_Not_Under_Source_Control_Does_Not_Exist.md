# What is not under source control doesn't exist

I remember the days long past where having your code under source control was not a given. In those dark times, even finding what was the "current" version of the sources was a perillous task.If you had multiple programmers, and if they had to work on different projects on the same code base at the same time... well, you had better pray that they wouldn't touch the same files at the.

Nowadays, a developper writing code that is not under source control is a laughting stock, and righfully so. Even the worst company understood take their source code is a precious asset, and that they should take steps to enshrine it into a SCM.

We use SCM because doing otherwise means that our code is not protected. It means that we don't know what happens to it. That we can't go back to any previous state. That we can't know how it evolved, who did what, and why.
In a world where "software" is the core of nearly every company, our code source is extremely valuable, so we take measure to protect if and to make the most out of it.

Code that is not under source control might has well not exist: you don't know exactly what it is or where it is.

So... why exactly should we treat other valuable things any differenly ?

## Wait, there is other valuable things apart from code ?

Yes, there is: it's mostly the things we need either to create our code in the first place, and the things we need to communicate about our code.

Here is some example of valuable things in a company:

* Documentation: your explanations about that complicated code implentation choice, and your user manual
* Specification: all those pesky things written business side telling your what you should create
* Presentations: those slides you used to present the next project architecture
* Uses cases definitions: that analysis you did with the business and the tester
* Meeting notes: those notes you scribbled on a piece of paper when you learned something important in a meeting
* Diagrams: that sequence diagram about an use case, and that architecture diagram explaining to management how are connected all your web apps

## They are already in an SCM ! My powerpoint is in a SVN repo !

Okay, can you then show me who changed that exact line, when, and why ?
Putting binary format in an SCM is (mostly) pointless. It's a step better than not having anything in a repository, but that's not much.

We have very powerfull tools to manage, edit, and track text file. As all that information is, in the end, text based, why exactly would you put it in a binary format that whould hid that simplicity from you ? There is so much to be gained by using a simple text format to store text information that I can't see the point of using anything else. Non tech people have at least the excuse of their ignorance (when the only thing you know is Word and Powerpoint, then you can't be blamed for using them), but you don't.

Have you ever seen, on th first page of a document, a list of all its author ? That list is updated by hand, by each successive author (if they think about it). Do you imagine doing that nowadays in your source code ? 

It's still common to receive a presentation by mail, making change to it, and send it back.. Would you accept the same process when developping software ?

Some binary type have a small source control system inside them. For example, Word allow you to track "revisions" of the document, and check for change.
However, it's clunky, and it's internal: you will still have many version of the doc, you will still send it to one another by email, ... It's not enough.

Putting all that data in text format in an SCM mean that you can, for example:

* Know who changed each line of the specification document, when, and why
* Revert the latest addition to a use case when it's proven false
* Track the change to the architecture design diagram
* Collaborate to edit your meeting notes (and easily finding it month later if needed !)

The additionnal goal here is to limit the duplication of the information, and to have only one central repository where all data can be found.
It also mean that when possible, documentation should be as near from its source as possible. For example, documentation that can be derived from the source code, or that have a strong link from the source code, should be stored with it (or even generated with it: [Living Documentation][Link to: living doc])

## Exemple of tools that can be used 

The most important point, in my opinion, is that all textual data (at least) should be stored in a textual format, and not in a binary format.
The actual format is less important: once it's textual, you will be able to easily switch from one format to another if needed.

Nonetheless, here is some of the tools I personnaly use:

### Documentation, Specifications, ...

I'm a big fan of Markdown for writing any kind of structured document. It give you all the benefits of the textual format:

* simple structure
* ability to use any editor you are confortable with (why wouldn't you write documentation in your IDE, after all ?)
* extreme portability

It also allow you to use powerful tools to transform that data, for example to style it in a nice way.
It's possible to use a static site generator like [Jekyll][http://www.jekyll.org] to build beautiful sites from your Markdown data.

Personnaly, I tend to use [Pandoc][http://www.pandoc.org] to generate [harmonised ? always the same style ?] HTML documents.

#### Markdown + Pandoc + HTML template example

todo: example markdown + simple template + pandoc usage

This very page is built with the same concept: todo link to bitbucket

### Presentations

I also use Markdown and Pandoc for writing my presentations. I've recently discovered an excellent library named "reveal.js" that allow you to create very nice slides from a simple HTML structure, and that there was a Pandow plugin to convert a Markdown document with a RevealJS Template.

#### Mardown + Pandow revealjs exeample

### Diagrams

graphivz, mermaid

