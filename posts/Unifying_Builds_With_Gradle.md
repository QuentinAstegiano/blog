# Unifying builds with Gradle

Your build process should be treated with the same care and respect than the rest of your code. If it's broken, hard to maintain or to expand, your software will suffer. As such, you should use the same concepts with your build process that you apply to your code.

That include:

* Everything should be under source control (and it's really "everything", even all those little scripts that you sometimes use)
* It should be tested
* It should be DRY

I'm going to focus here on the "DRY" part, because it's something that's rarely done, not hard to implement, and that bring a lot of benefits, especially if you have to manage more multiple projects.

You might have more than one project that is build in roughly the same way, for example:

* compiling from java sources
* unit testing
* acceptance testing
* integration testing
* executing some static analysis
* creating a release
* building some artifact (a library jar, a fat jar, a war, ...)
* publishing that artifact to a central repository

Having the same build logic in multiple projects is not DRY. It means that a lot of build code is duplicated across source code bases. It also means that each project will be built with a slightly different logic or syntax, precluding you from using common tools across projects.

Gradle allow you to easily create plugins. I will present you here how to create a simple Gradle plugin that will regroup all your common build logic, while still letting individual projects define their own logic if needed.

## Creating a gradle plugin

I will not dive here on the basic of plugin creation. You can find some extensive documentation [on the official site](https://docs.gradle.org/current/userguide/custom_plugins.html)
For my example, I will set it in the **buildSrc** folder of the project. You can find the complete working sources of the example on [Bitbucket](https://bitbucket.org/QuentinAstegiano/blog/src/master/posts/examples/UnifyingBuildsWIthGradle)

## Applying common configuration

Let's start with the basics, and use our plugin to apply the other generic plugins that we will need.
To keep things simple, let's says that our codebase is in java and in UTF-8:

{code:language=gradle:source=posts/examples/UnifyingBuildsWIthGradle/buildSrc/src/main/groovy/BuildPlugin.groovy#commonConfiguration}

I will also setup here Jacoco, as some configuration need to be done on each project.

## Repositories

{code:language=gradle:source=posts/examples/UnifyingBuildsWIthGradle/buildSrc/src/main/groovy/BuildPlugin.groovy#repositories}

The next step is to define the repositories that will be used by your projects.
You will probably want to include here either a central public repository, or a local repository that will mirror a central one. You might also want to reference a local repository that will host your privates artifacts.

## Tests

{code:language=gradle:source=posts/examples/UnifyingBuildsWIthGradle/buildSrc/src/main/groovy/BuildPlugin.groovy#tests}

I always have three categories of tests in my projects:

* some unit test that should be quick, and are executed after each commit
* some acceptance tests that check that the application does what it should be doing from a functional point of view
* some integration tests to validate that the application can work in a real situation, with actual external dependencies

I ensure that the naming pattern will be consistent across all my projects by defining those tasks here.

## Fat Jar 

{code:language=gradle:source=posts/examples/UnifyingBuildsWIthGradle/buildSrc/src/main/groovy/BuildPlugin.groovy#fatJar}

My projects tend to come into two primary forms: 

* a library that will be used in some other projects
* an application packaged as a fat jar

I distinguish here between the two by checking if there is a defined main class. 

## Publication

{code:language=gradle:source=posts/examples/UnifyingBuildsWIthGradle/buildSrc/src/main/groovy/BuildPlugin.groovy#publication}

Once you have your artifacts, you will want to configure how to publish them, either to a local private repository or to a central public one.
You will also want to publish the corrects artifacts for your projects.

## Release

{code:language=gradle:source=posts/examples/UnifyingBuildsWIthGradle/buildSrc/src/main/groovy/BuildPlugin.groovy#release}

I will not dive here into all the possibilities for release management in Gradle. I will keep things simple by applying [ajoberstar release plugin](https://github.com/ajoberstar/gradle-git/wiki/Release-Plugins). In a real world situation, you would probably want to define a lot more stuff here.

## Code analysis

{code:language=gradle:source=posts/examples/UnifyingBuildsWIthGradle/buildSrc/src/main/groovy/BuildPlugin.groovy#codeAnalysis}

The last thing I like to define in a common build plugin is the various static analysis tools.

Here, I setup some [SonarQube](http://www.sonarqube.org/) related configuration.

## Using the plugin in your project

Once all of this is done, the only thing left if to apply your plugin to your project.

{code:language=gradle:source=posts/examples/UnifyingBuildsWIthGradle/example-build.gradle}

The **buildConf** block is where you will define the project specific data needed by the plugin. You can use that mechanism to implement any kind of logic in your build.

If you have any specific task or configuration, it can be put in the **build.gradle** as usual.

In the end, all you need is a 10 lines Gradle script in each of your project to get a feature-complete java build ... not too bad for DRYness, I think ?

## Next steps

What I presented here is a simple view into what can be a build plugin. 

There is plenty of cases where using such a plugin does not make sense: 

* if you have either very few projects: because that would be a premature optimization
* or if you have very heterogeneous projects: because the plugin would be a mess of condition and complex logic

In my case, what I had was a couple dozen very similar applications (mostly REST services). I was able, using that logic, to mostly remove each build configuration, which was very nice.

There is also plenty of stuff I glossed over (in particular, the complexity coming with multi-modules projects, and the special case of the projects wanting to declare multiple artifacts). If that is your case, the best course of action would be to remove some functionality from the plugin (like the artifact publication) and only keep in it what is really common. Never try to factorize stuff that is too different!
