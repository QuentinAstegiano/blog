# The benefits of using a dumb template engine

there is a veriety of template engine: some allow you to write actual code in your page, some allow you to do mostly nothing, and everything in between
having a powerful template engine allow you to do complex things in your template
it means that you can write some logic into your template

not that long ago we used JSP, and we wrote SQL request right along an html tag
that code was rarely automaticaly tested:

testing the front end is time consuming, brittle, and painful
testing the back end tend to be a lot easier

if you have a template dumb enough, then you don't have to test it, because it's doing nothing but displaying a model

anything more complex than a for loop or a simple if (and I mean simple: "if this and that" is too complex) means that you can have bugs in the template

by reducing the template to it's simplest form you ensure that there can be no bug there

it means that the model you are giving to the template must contain all the logic, all the computation

## Example: a product page on an ecommerce website

requirements: 

* prices should be displayed in red if there's an active promotion 
* an "add to cart" button should be usable if the product is in stock
* a button allowing the user to suscribe to a "back in stock" alert should be usable if the product is not in stock

## Solution 1: smart template

Model contains:

* list of product promotion; each promotion contain a flag to indicate wether it's active or not
* the product current stock 

Template contains:

* An "active promotion check": loop on the promotions list, check if one of those is active, store the result in a flag
* A condition based on the computed promotion flag to pass the price in red
* A "product in stock check": check if the product stock is superior to 0, store the result in a flag
* A condition based on the computed stock flag to display the add to cart button 
* A condition based on the computed stock flag to display the "susbscribe to alert" button

## Solution 2: dump template

Model contains:

* a flag "that product has an active promotion"
* a flag "that product can be added to the cart"
* a flag "that product is eligible to the 'suscribe to alert'"

Template contains:

* A condition based on the "promotion flag"
* A condition based on the "product can be added to the cart"
* A condition based on the "product is eligible to the alert"

## Solution comparison

The "dumb" model will be a lot more verbose. For each rule that you have, you will need at least a flag. 
The "smart" model will contain less data (or at least, data that is less transformed / prepared).

What happen when requirement change ? For example, what if the new rule is "the 'subscribe to alert' button should only be displayed if the product is not definitly out of stock" ?

With the dumb model, the only thing you have to do is change the computation of the flag in your backend code. It's easy to test.
With the smart model, you have to change your template, possibily in multiple places (because templates doesn't make it easy to share logic). It's hard to test, and you are not really sure that you have changed it everywhere.

## Recommandations

Use a logic less template engine like Mustache instead of powerful tools like Freemarker or Thymeleaf (which are excellent products).
Keep your front end the dumber possible: what change on the front end should be the presentation, not the logic behind it. 
Separation of concern is important: logic less template help to enforce that.

## Limitations

Using a dump template is a well and good, but there's a massive elephant in that room: you can't do client side rendering with a dumb template. It means that building an heavy javascript application won't be possible under that paradigm. Doing ajax round trip everytime you want to update any piece of information is just not pratical, not effective enough.

I would then argue that in that case, you should go to the opposite end of the spectrum: a dumb backend exposing the purest data you can, and a smart front end containing all the logic (and all the tests !).
