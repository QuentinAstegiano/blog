# Keep things stupidy simple

What I see what we have to deal with everyday, as programmers, it seems to me that we **love** complexity.
Any software is an incredibly complex system. We learn to deal with that complexity (mostly by not deadling with it, lest it keeps up awake and screaming at night). We take it for granted: we just can't imagine a world where software is simple, small, and do precisely what it should be doing, and nothing more.

I think that our everyday tools and process are a lot more complicated than it should be. The software we develop tend to be over enginered.

Everyone know the joke about the entreprise software programmer [todo: insert link]
I think there's a lot of truth in that joke. Why does the expert programmer remove all that useless crap to only keep the core thing ? Because he know, deep down, that keeping only the code that is strictly necessary is the way to go. He knows that simplicity is a virtue.

Simplicity is not easiness. In fact, I would argue that simplicity tend to be harder to attain than complexity.
It's easy to {code}npm install left-pad{code}, it's easy to start a Spring Boot or a Dropwizard app, it's easy to use Hibernate to access your database.
It's much harder to use a tool that do only (and I mean **only**) what you want it to do, or to build one if none exist.

The hallmark of a good design is when nothing left to remove.

## Exemple: Publishing articles on a blog

I wanted for a very long time to have my very own blog - if only to collect my thoughts about design, software development, and a couple of other subjects.
I wondered what would be the best way for me to do that. I did have some simple requirements:

* The blog should serve html posts
* I need an easy interface to add, edit, and remove posts
* I want that all my articles content be versionned, with easy way to rollback some modifications
* I want to be able to edit my content, and save my changes, without publishing the article
* I want to be able to publish new content without linking it to the rest of the world before I decide it's ready

### Solution 1: use an existing solution

I could have used something like Blogger to host my pages.
It would fit my need: it should do everything I want, right ?

In fact, it would even do a lot more than what I want !

What if there is something I want to do in a different way than what is offered by the chosen platform ? What if I want to do something that is not allowed, like serving some complete, fully fledged and independent HTML static pages ? 

I am also very wary of any service that is free


[todo: link to rick video]

## Example

for example: publishing articles on a blog for the web to see
requirements:
* serving html posts
* interface to add, edit and remove posts
* versionning of the posts content
* "private" publication: content accessible for you to previsualize

### Solution 1: using an existing solution

TODO List existing solution

* a lot more functionnality that what you might need
* some functionnality might not be what you expect
* what if you need something that is not in that solution ?

### Solution 2: building yourself a solution mimicking the existing ones

* Backend: database + an access layer + an app
* A backoffice to manage the data
* Frontend: templates / angularJS / ...

=> That's a lot of work !

### Solution 3: a simple thing

#### How

* the posts are markdown documents, stored in bitbucket
* use of pandoc to convert markdown to html using a specific template
* transfer the html & style files via FTP to a basic Apache server

#### Consequences

* I started working on content right away
* setup time (only code written: html template & script to call pandoc and transfer files), overhead and cost is minimal
* total flexibility on the content I create : for example, I can easily (a template and a pandoc invocation) generate a pdf of the whole blog

will it be enough forever ? probably not
it'll be easy to add functionality as I need it
if someday I wish to use a specific existing solution, all I will have to do will be to convert my data: it's not strongly typed nor stored far away (imagine the hassle if your posts data are in a remote database under a specific, private format ?)

## [todo: titre]

we have a hard time thinking of simple solutions
we live in a complex world, and everything we do tend to be complicated
same thing when coding software: when faced with a problem, we might not think about an easy way to solve it, because we approach it thinking about how others did it, how we think the next step will need

recommandation: when thinking about a solution, think about everything you can remove from it - only when there is nothing left to remove you can procede
As long as something that is not absolutely needed to solve the issue remain, you can do better

doing otherwise is a premature optimization: you don't solve the problem at hand, but the problem you think you might have tomorrow
never try to solve a problem that you don't yet have, even if it seems to be clever
when solving a problem, if doing it "the smart way" take you a lot more code than doing it "the dumb way", the dumb way might be better (until proven otherwise)
it is much easier to remove something before creating it than afterward
once something is created it tend to stay in place

not creating something is much easier than removing it after it's inception
everything you create, you will be responsible for ever

in software, creation is easy - the hard way is that off the [limitation ? restriction ?]

simplicity is a lever for change
the most simple something is the easier it'll be able to evolve

the best software is the one that does not exist: with simple tools, you can solve problems without creating anything new
a simple thing can be moved / repurposed 
for example: this "blog" is built with roughly the same things I used to publish my CV

"nothing" is the simplest system
you have to add cmplexity for it to serve the purpose you need
be wary of each complexity you add, and don't add anymore than what is necessary
