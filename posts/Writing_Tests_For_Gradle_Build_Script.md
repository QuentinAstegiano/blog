# Writing tests for your Gradle build script

As far as I'm concerned, the scripts I use to build my projects are an integral part of the source code.

The next logical step is to treat them the same way I treat my code: that mean enforcing separation of concern, building reusable blocs wherever possible, and using TDD for mostly everything. I'm not advocating here to write unit test on a Gradle script that is only applying a couple of standard plugins and declaring some dependencies, but when your build become more complex (or more specific) I think it is a great idea to make use of Gradle powerful features.

I just want here to highlight a clever possibility given by Gradle to package your build script.

## Packaging your build script with Gradle: using BuildSrc

There's multiple ways to package new tasks and plugins for your build.
If everything is really simple, you can put them directly into your *build.gradle*. It's quick, it's easy, but it will be messy when things get complex, or when you want to do anything that's not trivial.

If you are building some really reusable blocks, the best way is to put them in a separate project that will be published on a standard repository. You can then treat that plugin as a fully fledged project, complete with tests, documentation, and continuous integration.

But sometimes (quite often, in my experience) what you need is to have some specific logic added to your build, something too complex to put into the *build.gradle*, but not reusable enough to deserve it's own project.

Luckily, Gradle offers a third way: you can put all your specific classes in a folder named *buildSrc* in the root directory of your project. Everything inside will be exposed to your build script, and only to your build script (it won't ever be packaged with your application, won't pollute your classpath, etc.).

The really clever point, and the one for which I did not find much documentation, is that **the buildSrc folder if a complete Gradle project**. It means (and that's really, really nice) that you can put a *build.gradle* file there, to store all of your project build dependencies and specifics.

For example, those blog posts are built from a Markdown source to HTML using some Gradle tasks defined that way.

Here is the content of the *buildSrc/build.gradle*:
{code:language=gradle:source=buildSrc/build.gradle}

It's quite simple, but it does allow me to specify some test dependencies for my build script, which mean I can write JUnit tests for my build logic, making me a happy man.
