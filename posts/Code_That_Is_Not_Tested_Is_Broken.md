# Code that is not tested is broken

## Why

code without proof might not work
at best you think it might work

if you are a smart developper, then you can hold all the code in your head, and check it that way. but what happen 6 month or 4 year after that ? do you still remember how and why things are done ? what happen if you have to work on some other people code ? do you prefer it with tests or not ? "leave the world the way it should be, not the way you found it"

imperfect human
100% test coverage is what should be done: anything less than that mean that your code is not proven to be working
any change become a leap of faith

testing provide proof that your code work
tests also provide an external definition of what the code does: whithout knowing how things should be done, how can you change them ?
tests enable changes to take place

## TDD as a test coverage guarantee

### What TDD is, according to me

TDD guarentee that your code is fully tested

TODO: Bob Martin rules about TDD
* don't write any line of production code that is not covered by an unit test
* don't write any line of test code beside what is necessary to get a failing test (not compiling count as failing)
* don't write any more line of production code that is necessary to make the failing test pass

this is only about production code: if you want to hack away, do some prototype or mockup, do it live... but discard that code as soon as you want something reliable (I would argue that's it's not that much longer to write test in the first place; if you have doubt about the TTL of the code you are writing, maybe you should TDD it... who never found himself with some "demo" code that made it's merry way into prodution ?)

### Testing pitfalls

TDD is not about writing test first and writing production code afterward: it's about doing both concurently
test only the boundaries of your code, not it's implementation detail: the idea behind testing is to allow changes to be easy to do. If the couple between your test code and your production code is too thight, then changing your production code make you change your test, and that's not the point !

tests should be of the higher possible level while still covering all your code possible outputs given all possible inputs
if some code path are hard to attain with your test, it might be a code smell

if you find yourself setting a method to package private in order to test it instead of testing the entry point of your class, that's a code smell: that method should probably be in its own class, that should be tested independently

### Testing as code smell finder

writing simple code make it much easier to test
if you have a hard time writing tests for your code, it's probably because your code can be bettered (sp ?)

it's easier to test stateless code without side effect than stateful code with side effect: the best case is to be able to fully test some code by checking it's output while controlling it's input

if you need to write 20 lines of setup code to put the world in the right place before writing a test, that's a code smell: it means that the code you are testing use more than what it is given (or that it's input are way too complicated)

