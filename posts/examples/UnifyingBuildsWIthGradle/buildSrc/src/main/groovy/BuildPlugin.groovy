import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.testing.Test
import org.gradle.api.publish.maven.MavenPublication

class BuildPlugin implements Plugin<Project> {

    void apply(Project rootProject) {
        rootProject.extensions.add("buildConf", BuildConfiguration)

        rootProject.allprojects.each { Project project ->
            commonConfiguration project
            repositories project
            tests project
        }
        rootProject.gradle.projectsEvaluated {
            fatJar rootProject
            publication rootProject
        }
        codeAnalysis rootProject
        release rootProject
    }

    private void commonConfiguration(Project project) {
        // Java
        project.plugins.apply("java")
        project.compileJava.options.encoding = "UTF-8"
        project.compileTestJava.options.encoding = "UTF-8"

        // Jacoco test coverage
        project.plugins.apply("jacoco")
        project.jacoco {
            toolVersion = "0.7.6.201602180812"
        }
        project.jacocoTestReport {
            additionalSourceDirs = project.files(project.sourceSets.main.allSource.srcDirs)
            sourceDirectories = project.files(project.sourceSets.main.allSource.srcDirs)
            classDirectories = project.files(project.sourceSets.main.output)
            reports {
                xml.enabled true
            }
        }
    }

    private void repositories(Project project) {
        project.repositories {
            maven {
                mavenLocal()
                mavenCentral()
            }
        }
    }

    private void tests(Project project) {
        // Unit testing
        project.test {
            exclude "**/*ITest*"
            exclude "**/*AcceptanceTest*"
        }
        // Integration testing
        project.task("integrationTest", type: Test) {
            exclude "**/*AcceptanceTest*"
            include "**/*ITest*"
        }
        // Acceptance testing
        project.task("acceptanceTest", type: Test) {
            exclude "**/*ITest*"
            include "**/*AcceptanceTest*"
        }
    }

    private void fatJar(Project project) {
        if (project.buildConf.mainClass != null) {
            project.plugins.apply("com.github.johnrengelman.shadow")
            project.shadowJar {
                manifest {
                    attributes 'Implementation-Title': project.buildConf.key
                    attributes 'Implementation-Version': project.version
                    attributes 'Main-Class': project.buildConf.mainClass
                }
                mergeServiceFiles()
                baseName = project.buildConf.key
                classifier = ""
                destinationDir = new File("$project.rootProject.buildDir/dist/")
            }
        }
    }

    private void publication(Project project) {
        project.plugins.apply("maven-publish")
        if (project.buildConf.mainClass != null) {
            project.publishing {
                repositories {
                    maven {
                        url "http://local.repository/repository/maven-releases"
                    }
                }
                publications {
                    shadowJar(MavenPublication) {
                        from project.components.shadow
                        artifactId project.buildConf.key
                        groupId project.group
                        version project.version
                    }
                }
            }
        } else {
            project.publishing {
                repositories {
                    maven {
                        url "http://local.repository/repository/maven-releases"
                    }
                }
                publications {
                    jar(MavenPublication) {
                        from project.components.java
                        artifactId project.buildConf.key
                        groupId project.group
                        version project.version
                    }
                }
            }
        }
    }

    private void release(Project project) {
        project.plugins.apply("org.ajoberstar.release-base")
    }

    private void codeAnalysis(Project project) {
        project.plugins.apply("org.sonarqube")
        project.sonarqube {
            properties {
                property "sonar.projectKey", project.buildConf.key
                property "sonar.projectName", project.buildConf.name
            }
        }
        project.getSubprojects().each {
            it.sonarqube {
                properties {
                    property "sonar.projectKey", "${project.buildConf.key}:${it.name}"
                }
            }
        }
    }

}

class BuildConfiguration {
    String key
    String name
    String mainClass
}
