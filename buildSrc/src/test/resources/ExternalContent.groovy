class ExternalContent {

    def someMethod() {
        someOtherMethod()

        def list = [ "a", "b", "ccc"]

        list.each {
            println it
        }
    }

    def someOtherMethod() {
        throw new UnsupportedOperationException()
    }
}
