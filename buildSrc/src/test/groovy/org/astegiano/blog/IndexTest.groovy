package org.astegiano.blog

import org.junit.Test

class IndexTest {

    @Test
    void should_create_index_from_a_collection_of_content() {
        // Given
        def contents = [ "content1", "content2", "content3" ]
        def index = new Index()

        // When
        def idx = index.create(contents)

        // Then
        assert idx != null
        assert idx.contains(contents[0])
        assert idx.contains(contents[1])
        assert idx.contains(contents[2])
    }

    @Test
    void should_create_index_from_a_collection_of_posts() {
        // Given
        def post1 = new PostFile("src/test/resources/testPost.md")
        def post2 = new PostFile("src/test/resources/testPost_2.md")
        def index = new Index()

        // When
        def idx = index.createFromPosts([post1, post2]);

        // Then
        assert idx != null
        assert idx.contains(post1.summary)
        assert idx.contains(post2.summary)
    }

    @Test
    void should_create_index_from_a_folder() {
        // Given
        def folder = new PostFiles("src/test/resources")
        def index = new Index()

        // When
        def idx = index.createFromFolder(folder)

        // Then
        assert idx != null
        folder.allPosts.each { post -> assert idx.contains(post.summary)}
    }
}
