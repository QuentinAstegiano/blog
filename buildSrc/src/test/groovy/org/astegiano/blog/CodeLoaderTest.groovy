package org.astegiano.blog

import org.junit.Test

class CodeLoaderTest {

    @Test
    void should_load_complete_external_file() {
        // Given
        def loader = new CodeLoader()
        // When
        def content = loader.load("src/test/resources/ExternalContent.groovy")
        // Then
        assert content.readLines() ==
"""class ExternalContent {

    def someMethod() {
        someOtherMethod()

        def list = [ "a", "b", "ccc"]

        list.each {
            println it
        }
    }

    def someOtherMethod() {
        throw new UnsupportedOperationException()
    }
}""".readLines()
    }

    @Test
    void should_load_external_file_with_a_line_range() {
        // Given
        def loader = new CodeLoader()
        // When
        def content = loader.load("src/test/resources/ExternalContent.groovy:L4,10")
        // Then
        assert content.readLines() ==
"""        someOtherMethod()

        def list = [ "a", "b", "ccc"]

        list.each {
            println it
        }""".readLines()
    }


    @Test
    void should_load_external_file_with_a_specific_method_method() {
        // Given
        def loader = new CodeLoader()
        // When
        def content = loader.load("src/test/resources/ExternalContent.groovy#someMethod")
        // Then
        assert content.readLines() ==
"""    def someMethod() {
        someOtherMethod()

        def list = [ "a", "b", "ccc"]

        list.each {
            println it
        }
    }""".readLines()
    }

    @Test
    void should_load_external_file_with_a_specific_method_method_and_only_pick_method_definition() {
        // Given
        def loader = new CodeLoader()
        // When
        def content = loader.load("src/test/resources/ExternalContent.groovy#someOtherMethod")
        // Then
        assert content.readLines() ==
"""    def someOtherMethod() {
        throw new UnsupportedOperationException()
    }""".readLines()
    }

}
