package org.astegiano.blog

import org.junit.Test

class PostFileTest {

    @Test(expected = IllegalArgumentException)
    void should_throw_exception_when_given_an_invalid_file() {
        // Given
        // When
        PostFile file = new PostFile("src/test/resources/not_existing.md")
        // Then
    }

    @Test
    void should_get_title_when_given_a_valid_file() {
        // Given
        def file = new File("src/test/resources/testPost.md")
        PostFile postFile = new PostFile(file)

        // When
        def title = postFile.title

        // Then
        assert title == "# Post title"
    }

    @Test
    void should_get_title_when_given_a_valid_file_name() {
        // Given
        PostFile file = new PostFile("src/test/resources/testPost.md")

        // When
        def title = file.title

        // Then
        assert title == "# Post title"
    }

    @Test
    void should_get_summary() {
        // Given
        PostFile file = new PostFile("src/test/resources/testPost.md")

        // When
        def summary = file.summary

        // Then
        assert summary == "# Post title\n\nsummary\nloren ipsum\n\n"
    }

    @Test
    void should_get_name() {
        // Given
        PostFile file = new PostFile("src/test/resources/testPost.md")
        // When
        def name = file.name
        // Then
        assert name == "testPost"
    }
}
