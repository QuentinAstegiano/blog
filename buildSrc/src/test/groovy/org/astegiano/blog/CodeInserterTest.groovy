package org.astegiano.blog

import org.junit.Test

class CodeInserterTest {

    @Test
    void should_not_change_data_if_no_code_block_is_present() {
        // Given
        def content = "<div>some content without any code block</div>"
        def inserter = new CodeInserter()
        // When
        def result = inserter.insertCode(content)
        // Then
        assert result == content
    }

    @Test
    void should_insert_multiline_code_block_with_one_is_found() {
        // Given
        def content = """<div>
            some content with a code block: 
            {code:language=java}
            System.out.println('hello');
            System.out.println('world');
            {/code}
            </div>"""
        def inserter = new CodeInserter()
        // When
        def result = inserter.insertCode(content)
        // Then
        def expected = """<div>
            some content with a code block: 
            <pre><code class='java'>
            System.out.println('hello');
            System.out.println('world');
            </code></pre>
            </div>"""
        assert result == expected
    }

    @Test
    void should_insert_inline_code_block_with_one_is_found() {
        // Given
        def content = """<div>
            some content with a code block: 
            {code-inline:language=java}System.out.println('hello'){/code-inline}
            </div>"""
        def inserter = new CodeInserter()
        // When
        def result = inserter.insertCode(content)
        // Then
        def expected = """<div>
            some content with a code block: 
            <pre><code class='java'>System.out.println('hello')</code></pre>
            </div>"""
        assert result == expected
    }

    @Test
    void should_insert_inline_code_block_with_multiple_are_found() {
        // Given
        def content = """<div>
            some content with a code block: 
            {code-inline:language=java}System.out.println('hello'){/code-inline}
            <br/>
            And some other code block:{code-inline:language=groovy}System.out.println('world'){/code-inline}
            </div>"""
        def inserter = new CodeInserter()
        // When
        def result = inserter.insertCode(content)
        // Then
        def expected = """<div>
            some content with a code block: 
            <pre><code class='java'>System.out.println('hello')</code></pre>
            <br/>
            And some other code block:<pre><code class='groovy'>System.out.println('world')</code></pre>
            </div>"""
        assert result == expected
    }

    @Test
    void should_insert_external_code_when_one_is_found() {
        // Given
        def content = """<div>
            some content with a code block: 
            {code:language=java:source=some/external/ref}
            </div>"""
        def loader = [load: { "System.out.println('hello external')" }]
        def inserter = new CodeInserter(codeLoader: loader)
        // When
        def result = inserter.insertCode(content)
        // Then
        def expected = """<div>
            some content with a code block: 
            <pre><code class='java'>System.out.println('hello external')</code></pre><div class='source-code-reference'><a target='blank' href='https://bitbucket.org/QuentinAstegiano/blog/src/master/some/external/ref'>See full sources</a></div>
            </div>"""
        assert result == expected
    }

    @Test
    void should_insert_multiple_internal_and_external_code() {
        // Given
        def content = """<div>
            <p>some content with an internal block: {code-inline:language=java}System.out.println('hello'){/code-inline}</p>
            some content with a code block: 
            {code:language=java:source=some/external/ref}
            <p>some other stuff</p>
            <br/>
            some content with an other internal block: {code-inline:language=java}System.out.println('world'){/code-inline}
            <br/>
            {code:language=java:source=some/external/ref2}
            </div>"""
        def loader = [load: { "System.out.println('hello external')" }]
        def inserter = new CodeInserter(codeLoader: loader)
        // When
        def result = inserter.insertCode(content)
        // Then
        def expected = """<div>
            <p>some content with an internal block: <pre><code class='java'>System.out.println('hello')</code></pre></p>
            some content with a code block: 
            <pre><code class='java'>System.out.println('hello external')</code></pre><div class='source-code-reference'><a target='blank' href='https://bitbucket.org/QuentinAstegiano/blog/src/master/some/external/ref'>See full sources</a></div>
            <p>some other stuff</p>
            <br/>
            some content with an other internal block: <pre><code class='java'>System.out.println('world')</code></pre>
            <br/>
            <pre><code class='java'>System.out.println('hello external')</code></pre><div class='source-code-reference'><a target='blank' href='https://bitbucket.org/QuentinAstegiano/blog/src/master/some/external/ref2'>See full sources</a></div>
            </div>"""

        assert result == expected
    }
}
