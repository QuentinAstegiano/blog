package org.astegiano.blog

import org.junit.Test

class PostFilesTest {

    @Test(expected = IllegalArgumentException)
    void should_throw_exception_when_given_an_invalid_folder() {
        // Given
        def files = new PostFiles("src/test/resources/not_existing")

        // When
        // Then
    }

    @Test
    void should_list_all_files_in_a_valid_folder() {
        // Given
        def files = new PostFiles("src/test/resources")

        // When
        def all = files.getAllPosts()

        // Then
        assert all.size() == 3
    }
}
