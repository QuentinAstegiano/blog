import org.astegiano.blog.Index
import org.astegiano.blog.PostFiles

def folder = new PostFiles("posts")
def summary = new Index().createFromFolder(folder)

def output = new File("build/posts")
output.mkdirs()

def summaryFile = new File("build", "index.md")
summaryFile.write(summary)

"pandoc -s -s build/index.md --template src/main/resources/template.html -o build/posts/index.html".execute()

folder.getAllPosts().each {
    "pandoc -s posts/${it.name}.md --template src/main/resources/template.html -o build/posts/${it.name}.html".execute()
}
