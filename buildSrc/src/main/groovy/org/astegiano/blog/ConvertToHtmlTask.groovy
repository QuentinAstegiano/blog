package org.astegiano.blog

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

/**
 * A simple task collecting all the posts and
 */
class ConvertToHtmlTask extends DefaultTask {

    @TaskAction
    void convertToHtml() {
        def folder = new PostFiles(project.buildDir.path + "/posts")
        def output = new File("${project.buildDir.path}/html-temp")
        output.mkdirs()
        folder.allPosts.each {
            def shell = "pandoc -s ${it.file.path} --template html/template.html -o ${output.path}/${it.name}.html".execute()
            shell.waitForProcessOutput()
        }
    }
}
