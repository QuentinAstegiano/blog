package org.astegiano.blog

/**
 * Load code from an external file.
 *
 * Accept a simple DSL to select what to load in the specified file
 */
class CodeLoader {

    String load(String expression) {
        def methodMatcher = (expression =~ /(.*)#(.*)$/)
        if (methodMatcher.matches()) {
            String content = ""
            methodMatcher.find {
                def completeData = fullFile(it[1].toString())
                boolean capture = false
                int indentationStartAt
                String pattern = it[2]
                completeData.eachLine { line ->
                    if (line.matches(/.*[a-zA-Z]* .*$pattern\(.*\{.*/)) {
                        capture = true
                        indentationStartAt = line.indexOf(line.trim()) // number of spaces before the first character
                    }
                    if (capture) {
                        content += line + "\n"
                        if (line.matches(/\s{$indentationStartAt}}/)) {
                            capture = false
                        }
                    }
                }
            }
            return content
        } else {
            def lineRangeMatcher = (expression =~ /(.*):L(\d+),(\d+)$/)
            if (lineRangeMatcher.matches()) {
                String content
                lineRangeMatcher.find {
                    Integer start = Integer.parseInt(it[2].toString()) - 1 // 0 index array
                    Integer end = Integer.parseInt(it[3].toString()) // 0 index array, but we include the last line
                    content = fullFile(it[1].toString()).readLines().subList(start, end).join("\n")
                }
                return content
            } else {
                fullFile(expression)
            }
        }
    }

    private String fullFile(String expression) {
        new File(expression).text
    }

}
