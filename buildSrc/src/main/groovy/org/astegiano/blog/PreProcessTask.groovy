package org.astegiano.blog

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

import java.nio.file.Files
import java.nio.file.StandardCopyOption

/**
 * Posts pre processing: copying to a build dir
 */
class PreProcessTask extends DefaultTask {

    @TaskAction
    void preprocess() {
        def folder = new PostFiles(project.rootDir.path + "/posts")
        def output = new File("${project.buildDir.path}/posts/")
        output.mkdirs()

        folder.allPosts.each { post ->
            Files.copy(
                    post.file.toPath(),
                    new File(output, post.file.name).toPath(),
                    StandardCopyOption.REPLACE_EXISTING)
        }
    }
}
