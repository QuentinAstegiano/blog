package org.astegiano.blog

class Index {

    String createFromFolder(PostFiles files) {
        createFromPosts(files.allPosts)
    }

    String createFromPosts(List<PostFile> files) {
        create(files.collect { file -> file.summary })
    }

    String create(List<String> contents) {
        contents.join("\n\n")
    }
}