package org.astegiano.blog

/**
 */
class PostFile {

    File file
    String name

    PostFile(File file) {
        this.file = file
        checkFile()
        this.name = file.name.replaceAll(/.md$/) { "" }
    }

    PostFile(String name) {
        this(new File(name))
    }

    private void checkFile() {
        if (!(file.exists() && file.isFile() && file.canRead())) {
            throw new IllegalArgumentException("File ${file.name} is either not found, not readable, or not a file")
        }
    }

    private List<String> read() {
        file.readLines("UTF-8")
    }

    String getTitle() {
        read().first()
    }

    String getSummary() {
        StringBuffer sb = new StringBuffer()
        boolean shouldRead = true
        read().each { line ->
            if (line.startsWith("##")) {
                shouldRead = false
            }
            if (shouldRead) {
                sb.append(line).append("\n")
            }
        }
        return sb.toString()
    }
}
