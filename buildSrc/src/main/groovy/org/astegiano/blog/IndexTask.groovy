package org.astegiano.blog

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

/**
 * Creating an index from all the posts
 */
class IndexTask extends DefaultTask {

    @TaskAction
    void createIndex() {
        def folder = new PostFiles(project.rootDir.path + "/posts")
        def summary = new Index().createFromFolder(folder)

        def output = new File(project.buildDir.path + "/posts")
        output.mkdirs()

        def summaryFile = new File(project.buildDir.path + "/posts", "index.md")
        summaryFile.write(summary)
    }
}
