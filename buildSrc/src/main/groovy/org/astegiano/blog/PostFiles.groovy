package org.astegiano.blog

class PostFiles {

    private File folder

    PostFiles(String name) {
        folder = new File(name)
        if (!(folder.exists() && folder.isDirectory() && folder.canRead())) {
            throw new IllegalArgumentException("Folder ${name} either does not exist, is not a directory, or is not readable")
        }
    }

    List<PostFile> getAllPosts() {
        List<PostFile> files = new ArrayList<>()
        folder.traverse { file ->
            if (file.name.endsWith(".md")) {
                files.add(new PostFile(file))
            }
        }
        return files
    }
}
