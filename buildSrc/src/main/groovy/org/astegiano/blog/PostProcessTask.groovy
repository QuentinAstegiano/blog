package org.astegiano.blog

import groovy.util.logging.Slf4j
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

/**
 * Posts post processing: inserting code fragments
 */
@Slf4j
class PostProcessTask extends DefaultTask {

    @TaskAction
    void preprocess() {
        def folder = new File("${project.buildDir.path}/html-temp/")
        log.info "Source folder: ${folder.path}, containing ${folder.list().size()} files"
        def output = new File("${project.buildDir.path}/html/")
        output.mkdirs()

        def inserter = new CodeInserter(codeLoader: new CodeLoader())
        folder.eachFile {
            def file = new File(output, it.name)
            log.info "Post processing file ${it.path} to output ${file.path}"
            file.write(inserter.insertCode(it.text))
        }
    }
}
