package org.astegiano.blog

/**
 *
 */
class CodeInserter {

    def codeLoader

    String insertCode(content) {
        def withInlineCode = insertInlineCode(content)
        def withExternalCode = insertExternalCode(withInlineCode)
        def withMultilineCode = insertMultilineCode(withExternalCode)
        return withMultilineCode
    }

    private String insertMultilineCode(data) {
        data.replaceAll(~/\{code:language=(.*)}/) { all, language ->
            "<pre><code class='$language'>"
        }.replaceAll(~/\{\/code}/) { all ->
            "</code></pre>"
        }
    }

    private String insertInlineCode(data) {
        data.replaceAll(~/\{code-inline:language=(.*)}(.*)\{\/code-inline}/) { all, language, code ->
            "<pre><code class='$language'>$code</code></pre>"
        }
    }

    private String insertExternalCode(data) {
         data.replaceAll(~/\{code:language=(.*):source=(.*)}/) { all, language, source ->
             def externalCode = codeLoader.load(source)
             def href = "https://bitbucket.org/QuentinAstegiano/blog/src/master/$source"
             "<pre><code class='$language'>$externalCode</code></pre><div class='source-code-reference'><a target='blank' href='$href'>See full sources</a></div>"
        }
    }
}
